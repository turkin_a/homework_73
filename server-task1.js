const express = require('express');
const app = express();
const port = 8000;

app.get('/:id', (req, res) => {
  res.send(`<h1 style="background: lightblue; text-align: center; padding: 20px">${req.params.id}</h1>`);
});

app.listen(port, () => {
  console.log('We are live on ' + port);
});