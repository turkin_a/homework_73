const express = require('express');
const Caesar = require('caesar-salad').Caesar;
const app = express();
const port = 8000;

const password = 'passphrase';
const styleEncode = '"background: lightpink; text-align: center; padding: 20px"';
const styleDecode = '"background: lightgreen; text-align: center; padding: 20px"';

app.get('/encode/:id', (req, res) => {
  const print = Caesar.Cipher(password).crypt(req.params.id);
  res.send(`<h1 style=${styleEncode}>${print}</h1>`);
});

app.get('/decode/:id', (req, res) => {
  const print = Caesar.Decipher(password).crypt(req.params.id);
  res.send(`<h1 style=${styleDecode}>${print}</h1>`);
});

app.listen(port, () => {
  console.log('We are live on ' + port);
});